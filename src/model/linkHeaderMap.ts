export type LinkHeaderMapEntry<V> = V[];

export class LinkHeaderMap extends Map<string, LinkHeaderMapEntry<URL>> {
    /**
     * Sets a link header in the map
     * @param key The  of the link
     * @param value The value of the link
     */
    public setLink(key: string, value: URL) {
        const v = this.get(key);
        v.push(value);
        return this.set(key, v);
    }

    /**
     * Returns the first link header of a specific type
     * @param key The key of the link
     */
    public getFirstLink(key: string): URL | undefined {
        return this.get(key)?.[0] || undefined;
    }

    public get(key: string) {
        return super.get(key) || new Array<URL>();
    }
}
