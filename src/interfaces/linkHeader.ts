export interface LinkHeader {
    rel: string;
    value: string;
    from?: string;
    until?: string;
    type?: string;
    datetime?: string;
}