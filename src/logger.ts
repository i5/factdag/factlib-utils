// tslint:disable-next-line: no-var-requires
// import log from 'loglevel';
//
//
//
// export const logger = {
//     verbose: function(msg: string) {
//         return log.trace(msg);
//     },
//     trace: log.trace,
//     info: log.info,
//     debug: log.debug,
//     log: log.debug,
//     warn: log.warn,
//     error: log.error,
// };

const { createLogger, format, transports } = require("winston");
const isNode = typeof process !== 'undefined' && process.release && /node|io\.js/.test(process.release.name);

const commonFormat = format.combine(
    format.timestamp({
        format: "YYYY-MM-DD HH:mm:ss",
    }),
    format.splat(),
    format.json(),
);

/**
 * Creates an instance of a Winston-Logger that is compatible for the browser.
 */
const createBrowserLogger = () => createLogger({
    defaultMeta: { service: "factlib.js" },
    format: commonFormat,
    level: "debug",
    transports: [
        new transports.Console({
            format: format.combine(
                format.colorize(),
                format.simple(),
                format.align(),
                format.printf((info: any) => `${info.timestamp} ${info.level}: ${info.message}`),
            ),
        })
    ],
});

/**
 * Creates an Instance of the Winston-Logger for NodeJS that is used to generate log files and console output.
 */
const createNodeJSLogger = () => {
    const logger = createLogger({
        defaultMeta: { service: "factlib.js" },
        format: format.combine(
            commonFormat,
            format.errors({ stack: true }),
        ),
        level: "debug",
        transports: [
            new transports.File({ filename: "factlib-error.log", level: "error" }),
            new transports.File({ filename: "factlib.log" }),
        ],
    });
    if (process.env.NODE_ENV !== "production") {
        logger.add(new transports.Console({
            format: format.combine(
                format.colorize(),
                format.simple(),
                format.align(),
                format.printf((info: any) => `${info.timestamp} ${info.level}: ${info.message}`),
            ),
        }));
    }
    return logger;
}

export const logger = isNode ? createNodeJSLogger() : createBrowserLogger();
