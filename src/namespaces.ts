// @ts-ignore
import * as rdf from "rdflib";

/**
 * Collection of used RDF-Namespaces
 */
export class Namespaces {
    public static PROV = new rdf.Namespace("http://www.w3.org/ns/prov#");
    public static RDF = new rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
    public static LDP = new rdf.Namespace("http://www.w3.org/ns/ldp#");
    public static MEM = new rdf.Namespace("http://mementoweb.org/ns#");
    public static FACT = new rdf.Namespace("http://www.factdag.org/ns#"); // TODO
    public static SC = new rdf.Namespace("http://schema.org/");
}

// tslint:disable-next-line:no-namespace
export namespace Namespaces {
    /**
     * Supported PROV-Relations.
     */
    export enum PROV_RELATION {
        WAS_ATTRIBUTED_TO = "wasAttributedTo",
        WAS_GENERATED_BY = "wasGeneratedBy",
        WAS_REVISION_OF = "wasRevisionOf",
        USED = "used",
        WAS_ASSOCIATED_WITH = "wasAssociatedWith",
        ACTED_ON_BEHALF_OF = "actedOnBehalfOf",
        HAD_PLAN = "hadPlan",
    }

    /**
     * Supported Types from the PROV model.
     */
    export enum PROV_TYPE {
        ENTITY = "Entity",
        ORGANIZATION = "Organization",
        AGENT = "Agent",
        SOFTWARE_AGENT = "SoftwareAgent",
        PERSON = "Person",
        ACTIVITY = "Activity",
        PLAN = "Plan",
        ASSOCIATION = "Association",
    }

    /**
     * Supported types of LDP resources.
     */
    export enum LDP_TYPES {
        CONTAINER = "Container",
        BASIC_CONTAINER = "BasicContainer",
        RDF_SOURCE = "RDFSource",
        NON_RDFSOURCE = "NonRDFSource",
    }

    /**
     * Supported LDP-Relations.
     */
    export enum LDP_RELATION {
        CONTAINS = "contains",
    }

    /**
     * Supported RDF-Relations.
     */
    export enum RDF_RELATION {
        TYPE = "type",
    }

    /**
     * Supported FACTDAG-Relations.
     */
    export enum FACT_RELATION {
        HAS_RESPONSIBLE_BROKER = "hasResponsibleBroker",
        OUTPUTS_TO = "outputsTo",
        HAS_OUTPUT_TYPE = "hasOutputType",
    }

    /**
     * Supported FACTDAG-Types.
     */
    export enum FACT_TYPE {
        BROKER = "Broker",
    }

    /**
     * Supported schema.org Relations.
     */
    export enum SCHEMA_RELATION {
        URL = "url",
        CODE_REPOSITORY = "SoftwareSourceCode#codeRepository",
    }

    /**
     * Supported Memento Relations.
     */
    export enum MEM_RELATION {
        MEMENTO = "memento",
        DATETIME = "mementoDatetime",
    }

}
