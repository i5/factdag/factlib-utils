import {LinkHeader} from "./interfaces/linkHeader";
import {LinkHeaderMap} from "./model/linkHeaderMap";

export const TIMEMAP = "timemap";
export const TIMEGATE = "timegate";
export const DESCRIBES = "describes";
export const TYPE = "type";
export const DESCRIBED_BY = "describedby";

export const parseLinkHeaders = (inputString: string): LinkHeader[] => {
    const linkHeaders = inputString.split('",');
    const result = Array<LinkHeader>();
    linkHeaders.forEach((element: string) => {
        element = element + '"';
        const elementArray = element.split("; ");
        const object: LinkHeader = { rel: "", value: "" };
        elementArray.forEach((item) => {
            if (item.startsWith("rel=")) {
                object.rel = item.split('"')[1];
            }
            if (item.includes("<")) {
                object.value = item.substring(item.lastIndexOf("<") + 1, item.lastIndexOf(">"));
            }
        });
        result.push(object);
    });
    return result;
};

export const getLastMementoFromLinkHeader = (inputString: string): string => {
    let result = "";
    parseLinkHeaders(inputString).forEach((element) => {
        if (element.rel === "last memento" || element.rel === "first last memento") {
            result = element.value;
        }
    });
    return result;
};

export const getTimegateFromLinkHeader = (inputString: string): string => {
    let result = "";
    parseLinkHeaders(inputString).forEach((element) => {
        if (element.rel === "timegate" || element.rel === "original timegate") {
            result = element.value;
        }
    });
    return result;
};

export const getTimemapFromLinkHeader = (inputString: string): string => {
    let result = "";
    parseLinkHeaders(inputString).forEach((element) => {
        if (element.rel === "timemap") {
            result = element.value;
        }
    });
    return result;
};

export const getDescribedFromLinkHeader = (inputString: string): string => {
    let result = "";
    parseLinkHeaders(inputString).forEach((element) => {
        if (element.rel === DESCRIBED_BY) {
            result = element.value;
        }
    });
    return result;
};

export const buildLinkHeadersMap = (linkHeaderString: string): LinkHeaderMap => {
    const linkHeaders = new LinkHeaderMap();
    const timemapString = getTimemapFromLinkHeader(linkHeaderString);
    const timegateString = getTimegateFromLinkHeader(linkHeaderString);
    const describedBy = getDescribedFromLinkHeader(linkHeaderString);

    if (timemapString) {
        linkHeaders.setLink(TIMEMAP, new URL(timemapString));
    }
    if (timegateString) {
        linkHeaders.setLink(TIMEGATE, new URL(timegateString));
    }

    if(describedBy) {
        linkHeaders.setLink(DESCRIBED_BY, new URL(describedBy));
    }



    const typeHeaders = parseLinkHeaders(linkHeaderString).
        filter((value) => value.rel === TYPE).
        map((value) => new URL(value.value));
    linkHeaders.set(TYPE, typeHeaders);
    return linkHeaders;
};
